﻿using Microsoft.AspNetCore.Mvc;
using Parking.API.Models;
using Parking.API.Services;

namespace Parking.API.Controllers
{
	[Route("api/[controller]")]
	[Produces("application/json")]
	[ApiController]
    public class CarController : ControllerBase
    {
		private readonly IParkingService _parking;

		public CarController(IParkingService parking)
		{
			_parking = parking;
		}

		[HttpGet("all")]
		public IActionResult ShowAllCars()
		{
			return Ok(_parking.ShowAllCars());
		}

		[HttpPost("add")]
		public IActionResult AddCar([FromBody] Car car)
		{
			if (_parking.GetCountOfCars() == Setting.MaxCountOfParkingPlaces)
				return BadRequest(); 

			_parking.AddCar(car);

			return Ok(StatusCode(200));
		}

		[HttpDelete("delete/{id}")]
		public IActionResult DeleteCar(int id)
		{
			if (_parking.IsCarExist(id))
			{
				_parking.DeleteCar(id);
				return Ok(StatusCode(200));
			}
			else
				return BadRequest();
		}

		[HttpPut("edit/{id}/{money}")]
		public IActionResult EditCar(int id, decimal money)
		{
			if (_parking.IsCarExist(id))
			{
				_parking.TopUpCarBalance(id, money);
				return Ok(StatusCode(200));
			}
			else
				return BadRequest();
		}
	}
}