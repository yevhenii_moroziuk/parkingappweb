﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Parking.API.Models;
using Parking.API.Services;

namespace Parking.API.Controllers
{
	[Route("api/[controller]")]
	[Produces("application/json")]
	[ApiController]
    public class ParkingController : ControllerBase
    {
		private readonly IParkingService _parking;

		public ParkingController(IParkingService parking)
		{
			_parking = parking;
		}

		[HttpGet("balance")]
		public ActionResult<decimal> GetBalance()
		{
			return Ok( _parking.GetBalance());
		}

		[HttpGet("income")]
		public ActionResult<decimal> IncomeForLastMinute()
		{
			return Ok(_parking.GetIncomeForOneMinute());
		}

		[HttpGet("places")]
		public ActionResult<string> CountOfFreePlaces()
		{
			(int total, int free) = _parking.GetCountOfFreePlaces();
			return Ok($"Total: {total}, free: {free}");
		}
	}
}