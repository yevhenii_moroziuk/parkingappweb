﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Parking.API.Models;
using Parking.API.Services;

namespace Parking.API.Controllers
{
	[Route("api/[controller]")]
	[Produces("application/json")]
	[ApiController]
    public class TransactionController : ControllerBase
    {
		private readonly IParkingService _parking;

		public TransactionController(IParkingService parking)
		{
			_parking = parking;
		}

		[HttpGet("last")]
		public ActionResult<IEnumerable<Transaction>> TransactionForLastMinute()
		{
			return Ok(_parking.ShowAllTransactionsForLastMinute());
		}

		[HttpGet("all")]
		public ActionResult<IEnumerable<string>> AllTransctions()
		{
			return Ok(_parking.ShowAllTransactions());
		}
	}
}