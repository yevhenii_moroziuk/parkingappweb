﻿namespace Parking.API.Models
{
	public class Car
	{
		private static int _countOfCars = 0;
		public int CarId { get; private set; }
		public decimal CurrentBalance { get; set; }
		public VehicleType Type { get; private set; }
		public Car(VehicleType vehicleType, decimal defaultBalance )
		{
			CarId = _countOfCars++;
			CurrentBalance = defaultBalance;
			Type = vehicleType;
		}
		public override string ToString()
		{
			return $"Car id: {CarId}, Car type: {Type}, Car Balance: {CurrentBalance}";
		}
	}
}
