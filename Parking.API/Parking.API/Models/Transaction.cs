﻿using System;

namespace Parking.API.Models
{
	public class Transaction
	{
		private static int _countOfTransactions = 0;

		public int TransactionId { get; }

		public decimal TransactionSum { get; set; }

		public DateTime TransactionDate { get; }

		public Transaction(decimal sum)
		{
			TransactionId = _countOfTransactions;
			TransactionSum = sum;
			TransactionDate = DateTime.Now;
			_countOfTransactions++;
		}
		public override string ToString()
		{
			return $"Transaction id: {TransactionId},Transaction date: {TransactionDate}, Transaction sum: {TransactionSum}";
		}

	}
}
