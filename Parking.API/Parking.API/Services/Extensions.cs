﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parking.API.Services
{
	public static class Extensions
	{
		public static void AddParkingService(this IServiceCollection service)
		{
			service.AddSingleton<IParkingService, ParkingWeb>();
		}
	}
}
