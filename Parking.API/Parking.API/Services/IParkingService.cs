﻿using Parking.API.Models;
using System.Collections.Generic;

namespace Parking.API.Services
{
	public interface IParkingService
	{

		decimal GetIncomeForOneMinute();
		(int, int) GetCountOfFreePlaces();
		IEnumerable<Transaction> ShowAllTransactionsForLastMinute();
		IEnumerable<string> ShowAllTransactions();
		IEnumerable<Car> ShowAllCars();
		void AddCar(Car car);
		void DeleteCar(int id);
		void TopUpCarBalance(int id, decimal money);
		decimal GetBalance();
		bool IsCarExist(int id);
		int GetCountOfCars();
	}
}
