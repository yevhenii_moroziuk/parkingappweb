﻿using Parking.API.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Parking.API.Services
{
	class ParkingWeb : IParkingService
	{
		private static decimal _balance;
		private static List<Car> _cars;
		private static List<Transaction> _transactions;
		private string _filePath;
		private int _transactionTimeOut = Setting.TransactionInterval;
		private int _loggingTimeOut = Setting.LoggingInterval;

		private TimerCallback timerCallback1, timerCallback2;
		private Timer timer1, timer2;

		public ParkingWeb()
		{
			_balance = Setting.DefaultBalance;
			_cars = new List<Car>();
			_transactions = new List<Transaction>();
			_filePath = Setting.FilePath;
		}

		public void AddCar(Car car)
		{
			_cars.Add(car);

			if (_cars.Count() > 0 && timerCallback1 == null && timerCallback2 == null)
			{
				timerCallback1 = new TimerCallback(CreateTransaction);
				timer1 = new Timer(timerCallback1, null, _transactionTimeOut, _transactionTimeOut);

				timerCallback2 = new TimerCallback(WriteTransactionsToLog);
				timer2 = new Timer(timerCallback2, null, _loggingTimeOut, _loggingTimeOut);
			}
		}

		private static void CreateTransaction(object o)
		{
			if (_cars.Count > 0)
			{
				decimal transactionSum = 0;

				foreach (Car car in _cars)
				{
					transactionSum += Pay(car);
				}

				_transactions.Add(new Transaction(transactionSum));

				_balance += transactionSum;
			}
		}

		private static decimal Pay(Car car)
		{
			decimal payment = 0;

			if (car.CurrentBalance - Setting.Tarif[car.Type.ToString()] < 0)
			{
				payment += Setting.PenaltyCoeff * Setting.Tarif[car.Type.ToString()];
			}
			else
			{
				payment += Setting.Tarif[car.Type.ToString()];
			}

			car.CurrentBalance -= payment;

			return payment;
		}

		public void DeleteCar(int id)
		{
			var car = _cars.SingleOrDefault(c => c.CarId == id);
			_cars.Remove(car);
		}

		public decimal GetBalance()
		{
			return _balance;
		}

		public (int, int) GetCountOfFreePlaces()
		{
			return (Setting.MaxCountOfParkingPlaces, Setting.MaxCountOfParkingPlaces - _cars.Count());
		}

		public decimal GetIncomeForOneMinute()
		{
			decimal income = 0;
			foreach (Transaction transaction in _transactions)
			{
				income += transaction.TransactionSum;
			}
			return income;
		}

		public IEnumerable<Car> ShowAllCars()
		{
			return _cars;
		}

		public IEnumerable<string> ShowAllTransactions()
		{
			try
			{
				List<string> temp = new List<string>();
				using (StreamReader r = File.OpenText(_filePath))
				{
					string line;
					while ((line = r.ReadLine()) != null)
					{
						temp.Add(line);
					}
				};
				return temp;

			}
			catch
			{
				return null;
			}
		}

		public IEnumerable<Transaction> ShowAllTransactionsForLastMinute()
		{
			return _transactions;
		}

		public void TopUpCarBalance(int id, decimal money)
		{
			var car = _cars.FirstOrDefault(c => c.CarId == id);
			car.CurrentBalance += money;

		}

		public void WriteTransactionsToLog(object str)
		{
			if (_cars.Count() > 0)
			{
				try
				{
					string text = $"Time: {DateTime.Now.ToLocalTime()}, Sum: {GetIncomeForOneMinute()}";
					using (var w = new FileStream(_filePath, FileMode.OpenOrCreate))
					{
						w.Seek(0, SeekOrigin.End);
						w.Write(Encoding.Default.GetBytes(text + Environment.NewLine));
					}
					_transactions.Clear();
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}
			}

		}

		public bool IsCarExist(int id)
		{
			var car = _cars.FirstOrDefault(c => c.CarId == id);
			return car == null ? false : true;
		}

		public int GetCountOfCars() => _cars.Count();
	}
}
