﻿using System.Collections.Generic;

namespace Parking.API
{
	public static class Setting
	{
		public static decimal DefaultBalance = 0;

		public static int MaxCountOfParkingPlaces = 10;

		public static int TransactionInterval = 5000;

		public static int LoggingInterval = 60000;

		public static IDictionary<string, decimal> Tarif = new Dictionary<string, decimal>()
		{
			{ "Car", 2m },
			{"Bus", 3.5m },
			{"Truck", 5m },
			{ "Motorcylce", 1m }
		};

		public static decimal PenaltyCoeff = 2.5m;

		public static string FilePath = "log.txt";
	}
}
