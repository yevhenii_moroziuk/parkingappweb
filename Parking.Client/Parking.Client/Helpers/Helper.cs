﻿using System;

namespace Parking.Client.Helpers
{
	public class Helper
	{
		public static void ShowInfo()
		{
			Console.WriteLine("+--------------------------------------------+");
			Console.WriteLine("| 1 - Show income for last minute            |");
			Console.WriteLine("| 2 - Show count of free parking places      |");
			Console.WriteLine("| 3 - Show all transactions for last minute  |");
			Console.WriteLine("| 4 - Show full transactions history         |");
			Console.WriteLine("| 5 - Show all vehicles                      |");
			Console.WriteLine("| 6 - Add vehicle                            |");
			Console.WriteLine("| 7 - Delete vehicle                         |");
			Console.WriteLine("| 8 - Top up the balance on specific vehicle |");
			Console.WriteLine("| 9 - Show parking balance                   |");
			Console.WriteLine("| 10- Exit                                   |");
			Console.WriteLine("+--------------------------------------------+");
		}

		public static int InputInteger(int left, int right)
		{
			int input;
			while (!(int.TryParse(Console.ReadLine(), out input) && (input >= left && input <= right)))
			{
				if (input == -1)
					return -1;

				Console.WriteLine("folow the instraction please");
			}
			return input;
		}

		public static int InputInteger()
		{
			int input;
			while (!(int.TryParse(Console.ReadLine(), out input)))
			{
				if (input == -1)
					return -1;

				Console.WriteLine("folow the instraction please");
			}
			return input;
		}

		public static decimal InputDecimal()
		{
			decimal input;
			while (!(decimal.TryParse(Console.ReadLine(), out input) && input>0))
			{
				Console.WriteLine("folow the instraction please");
			}
			return input;
		}
		public static void InMenuMessage()
		{
			Console.WriteLine("Press any key to continue");
			Console.ReadKey();
			Console.Clear();
		}

		public static void PrintColoredMessage(string message, ConsoleColor color)
		{
			Console.ForegroundColor = color;
			Console.WriteLine(message);
			Console.ResetColor();
		}
	}
}
