﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Client
{
	class Library
	{
		private	readonly HttpClient _client;
		private string _base;

		public Library()
		{
			_client = new HttpClient();
			_base = Setting.Address;
		}
		public async Task<T> Get<T>(string addres)
		{
			var result = await _client.GetStringAsync(_base + addres);
			return JsonConvert.DeserializeObject<T>(result);
		}

		public async Task AddCar(int type, decimal balance)
		{
			HttpRequestMessage request = new HttpRequestMessage()
			{
				Content = new StringContent("{vehicletype: " + type + ",defaultbalance: " + balance + "}",
				Encoding.UTF8, "application/json"),
				Method = HttpMethod.Post,
				RequestUri = new Uri(_base+ "car/add")
			};

			if ((await _client.SendAsync(request)).IsSuccessStatusCode)
				Console.WriteLine("added");
			else
				Console.WriteLine("Could`t add");
		}

		public async Task EditCar(int id, decimal money)
		{
			HttpRequestMessage request = new HttpRequestMessage()
			{
				Method = HttpMethod.Put,
				RequestUri = new Uri(_base + $"car/edit/{id}/{money}")
			};

			if ((await _client.SendAsync(request)).IsSuccessStatusCode)
				Console.WriteLine("edited");
			else
				Console.WriteLine("Could`t edit");
		}

		public async Task DelCar(int id)
		{
			HttpRequestMessage request = new HttpRequestMessage()
			{
				Method = HttpMethod.Delete,
				RequestUri = new Uri(_base + $"car/delete/{id}")
			};

			if ((await _client.SendAsync(request)).IsSuccessStatusCode)
				Console.WriteLine("deleted");
			else
				Console.WriteLine("Could`t delete");
		}
	}
}
