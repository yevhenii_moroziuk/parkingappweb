﻿using Newtonsoft.Json;

namespace Parking.Client.Models
{
	public class Car
	{
		[JsonProperty("carid")]
		public int CarId { get; private set; }

		[JsonProperty("currentbalance")]
		public decimal CurrentBalance { get; set; }

		[JsonProperty("type")]
		public VehicleType Type { get; private set; }

		public override string ToString()
		{
			return $"Car id: {CarId}, Car type: {Type}, Car Balance: {CurrentBalance}";
		}
	}
}
