﻿using Newtonsoft.Json;
using System;

namespace Parking.Client.Models
{
	public class Transaction
	{
		[JsonProperty("transactionid")]
		public int TransactionId { get; set; }

		[JsonProperty("transactionsum")]
		public decimal TransactionSum { get; set; }

		[JsonProperty("transactiondate")]
		public DateTime TransactionDate { get; set; }

		public override string ToString()
		{
			return $"Transaction id: {TransactionId},Transaction date: {TransactionDate.ToLongTimeString()}, Transaction sum: {TransactionSum}";
		}

	}
}
