﻿using Parking.Client.Helpers;
using Parking.Client.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Parking.Client
{
	class Program
	{
		private static Library _parking = new Library();


		static async Task Main(string[] args)
		{

			bool _working = true;
			bool _connected = true;

			try
			{
				await _parking.Get<string>("parking/income");
			}
			catch
			{
				_connected = false;
			}
			if (!_connected)
				Console.WriteLine("Can`t connect to the server");
			else
				while (_working)
				{
					Helper.ShowInfo();
					int input = Helper.InputInteger(1, 10);

					switch (input)
					{
						case 1:
							await IncomeAsync();
							break;
						case 2:
							await ParkingPlacesAsync();
							break;
						case 3:
							await TransactionsLastAsync();
							break;
						case 4:
							await TransactionsAllAsync();
							break;
						case 5:
							await AllVehiclesAsync();
							break;
						case 6:
							await AddCarAsync();
							break;
						case 7:
							await RemoveCarAsync();
							break;
						case 8:
							await EditCarAsync();
							break;
						case 9:
							await BalanceAsync();
							break;
						case 10:
							_working = false;
							break;
					}
				}
		}

		private static async Task IncomeAsync()
		{
			Console.WriteLine("Income for last minute: ");
			Console.WriteLine(await _parking.Get<string>("parking/income"));
			Helper.InMenuMessage();
		}

		private static async Task ParkingPlacesAsync()
		{
			Console.WriteLine($"Count of parking places:");
			Console.WriteLine(await _parking.Get<string>("parking/places"));
			Helper.InMenuMessage();
		}

		private static async Task TransactionsLastAsync()
		{
			Console.WriteLine("All transactions for last minute:");
			var lastTransactions = await _parking.Get<List<Parking.Client.Models.Transaction>>("transaction/last");
			if (lastTransactions != null)
				foreach (var item in lastTransactions)
				{
					Console.WriteLine(item.ToString());
				}
			else
				Console.WriteLine("nothing");
			Helper.InMenuMessage();
		}

		private static async Task TransactionsAllAsync()
		{
			Console.WriteLine("Full transactions history: ");
			var transactions = await _parking.Get<List<string>>("transaction/all");
			if (transactions != null)
				foreach (var item in transactions)
				{
					Console.WriteLine(item);
					Console.WriteLine(Setting.LogSepartor);
				}
			else
				Console.WriteLine("nothing");
			Helper.InMenuMessage();
		}

		private static async Task AllVehiclesAsync()
		{
			Console.WriteLine("All vehicles:");
			var cars = await _parking.Get<List<Car>>("car/all");
			if (cars != null)
				foreach (var item in cars)
				{
					Console.WriteLine(item.ToString());
				}
			else
				Console.WriteLine("nothing");
			Helper.InMenuMessage();
		}

		private static async Task AddCarAsync()
		{
			Helper.PrintColoredMessage("Add car:", ConsoleColor.Green);
			Console.WriteLine("Input 1 - Car, 2 - Bus, 3 - Truck, 4 - Motorcycle. Press -1 to leave");

			int carType = Helper.InputInteger(1, 4);
			if (carType == -1)
			{
				Console.Clear();
				return;
			}

			Console.WriteLine("Input car balance");
			decimal balance = Helper.InputDecimal();

			await _parking.AddCar(carType, balance);
			Helper.InMenuMessage();
		}

		private static async Task RemoveCarAsync()
		{
			Helper.PrintColoredMessage("Delete vehicle:", ConsoleColor.Red);
			Console.WriteLine("Input car id. Press -1 to leave");

			int id = Helper.InputInteger();
			if (id == -1)
			{
				Console.Clear();
				return;
			}
			await _parking.DelCar(id);
			Helper.InMenuMessage();
		}

		private static async Task EditCarAsync()
		{
			Helper.PrintColoredMessage("Top up the balance on specific vehicle:", ConsoleColor.Blue);
			Console.WriteLine("Input car id. Press -1 to leave");

			int carid = Helper.InputInteger();
			if (carid == -1)
			{
				Console.Clear();
				return;
			}
			Console.WriteLine("Input car balance");
			decimal money = Helper.InputDecimal();

			await _parking.EditCar(carid, money);
			Helper.InMenuMessage();
		}

		private static async Task BalanceAsync()
		{
			Console.Write("Parking balance: ");
			Console.WriteLine(await _parking.Get<string>("parking/balance"));
			Helper.InMenuMessage();
		}
	}
}

