﻿using System;
using System.Collections.Generic;

namespace Parking.Client
{
	public static class Setting
	{
		public static IDictionary<string, decimal> Tarif = new Dictionary<string, decimal>()
		{
			{ "Car", 2m },
			{"Bus", 3.5m },
			{"Truck", 5m },
			{ "Motorcylce", 1m }
		};

		public static string Address = "https://localhost:44320/api/";

		public static string LogSepartor = "--------------------------------------------------" + Environment.NewLine;
	}
}
