# ParkingAppWeb

This is client/server version of ParkingApp

Задания необходимо выполнять в Bitbucket репозиториях (не Github)
---
Эта домашка основана на предыдущей. В лекции я показал вам как создавать простые WebApi приложения,
поэтому вам необходимо будет сделать подобное. В результате выполнения домашки, у вас должно получиться 
одно консольное приложение (я его буду называть "клиент") и одно WebApi приложение (это будет "сервер").
Суть работы заключается в том, чтобы воспроизвести типичный подход к построению систем приложений клиент-cервер. 
Итак, что необходимо перенести с консольного приложения на сервер:

1. Выполнение транзакций (то есть раз в N секунд нужно собирать деньги с ТС)
2. Запись транзакций в файл
3. Класс с настройками
4. Типы транспортных средств
5. Остальные классы, такие как парковка и ТС.

Далее необходимо создать набор контроллеров и сервисов, для того чтобы клиент мог обмениваться данными с сервером. В API нужно реализовать все функциональные требования с прошлой домашки (такие как узнать баланс парковки, заработанную сумму денег, и т.д.).
Клиент (консольное приложение) должен уметь отправлять HTTP запросы на сервер для того, чтобы получать и отправлять данные и управлять парковкой.
Например, пользователь запускает консольное приложение и видит перед собой список действий, которые он может выполнить. Он выбирает “Узнать сумму заработанных денег за последнюю минуту” -> с клиента отправляется HTTP запрос на API сервера, который возвращает клиенту нужную информацию.

Рекомендации к выполнению:

1. Старт парковки должен быть реализован в классе Startup
2. Можно использовать HttpClient для отправки запросов с клиента
3. Используйте JSON как формат данных для общения между приложениями
4. Старайтесь не усложнять (не нужно подключать базу данных, заморачиваться с асинхронностью и т.д.)

